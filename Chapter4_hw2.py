#Example 1:
from math import pi
def circ_area_in_sq(a):
    a = float(a)
    return (pi*((a/2)**2))

# testing 1:
print(circ_area_in_sq(0))
print(circ_area_in_sq(1))
print(circ_area_in_sq(2))

#output
#0.0
#0.7853981633974483
#3.141592653589793

'''================================================================================================================'''

#Example 2:
from math import pi
def circ_area_in_sq(a):
    try:
        a = float(a)
        return (pi*((a/2)**2))
    except ValueError:
        return None

# testing 2:
print(circ_area_in_sq("not a number"))
print(circ_area_in_sq(3))
print(circ_area_in_sq(4))

#output
#None
#7.0685834705770345
#12.566370614359172