#cung cấp các hàm và các biến được sử dụng để thao tác các phần khác nhau 
#của môi trường chạy Python. Nó cho phép chúng ta truy cập các tham số và 
#chức năng cụ thể của hệ thống.
import sys

# count up 
def countup(n):
  if n >= 0:
    print('Blastoff!')
  else:
    print(n)
    countup(n+1)


# count down 
def countdown(n):
  if n <= 0:
    print('Blastoff!')
  else:
    print(n)
    countdown(n-1)

# Please user to enter number 
if sys.version_info[0] == 3:
  num = input('Enter your number: ')
else:
  num = raw_input('Enter your number: ')

num = int(num)

if num > 0:
  countdown(num)
elif num < 0:
  countup(num)
else:
  print('Blastoff!')

# Outputs:
# num=3    3 2 1 Blastoff!
# num=-3   -3 -2 -1 Blastoff!
# num=0    Blastoff!
