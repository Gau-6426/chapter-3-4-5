def hypotenuse(leg1,leg2): 
    return (leg1**2+leg2**2)**(1/2) 
                                   
  
print("Test number 1, triangle with legs 3 and 4, hypotenuse :",hypotenuse(3,4))
print("Expected 5")
print("Test number 1, triangle with legs 12 and 5, hypotenuse :",hypotenuse(12,5))
print("Expected 13")
print("Test number 1, triangle with legs 20 and 21, hypotenuse :",hypotenuse(20,21))
print("Expected 29")

#output
#Test number 1, triangle with legs 3 and 4, hypotenuse : 5.0
# Expected 5
#Test number 1, triangle with legs 12 and 5, hypotenuse : 13.0
# Expected 13
#Test number 1, triangle with legs 20 and 21, hypotenuse : 29.0
#Expected 29